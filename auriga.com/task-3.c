// Copyright 2018 Dmitry Bogatov <KAction@gnu.org>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/* Develop your version of the ls utility. Only the 'ls -l'
 * functionality is needed. 
 */

// The most oblivious interpretation of task is to use GNU libc, so let
// it be. I would prefer Diet libc and libowfat (www.fefe.de).
//
// Oh, and by the way, I interpret that this program need not support
// any options, and should only behave like 'ls -l'.

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <fcntl.h>
#include <stdlib.h>

// Should suffice. File a bug report, if in your locale formatting of
// date and time requires more space.
enum { DATETIME_BUFSIZE = 100 }; 

struct perm_s {
	char s[10];
};

static char
filetype_marker(mode_t mode)
{
	switch (mode & S_IFMT) {
	case S_IFDIR:  return 'd';
	case S_IFCHR:  return 'c';
	case S_IFBLK:  return 'b';
	case S_IFIFO:  return 'p';
	case S_IFLNK:  return 'l';
	case S_IFSOCK: return 's';
	case S_IFREG:  return '-';
	default:
		assert("Unknown file type" && 0);
		return '?'; /* Not reached in debug build */
	}
}

static void
format_permissions(struct perm_s *perm, mode_t mode)
{
	unsigned int index;

	index = 0;

#define FILL_PERM(bit, sym) perm->s[index++] = (mode & bit) ? sym : '-';
	FILL_PERM(S_IRUSR, 'r');
	FILL_PERM(S_IWUSR, 'w');
	FILL_PERM(S_IXUSR, 'x');

	FILL_PERM(S_IRGRP, 'r');
	FILL_PERM(S_IWGRP, 'w');
	FILL_PERM(S_IXGRP, 'x');

	FILL_PERM(S_IROTH, 'r');
	FILL_PERM(S_IWOTH, 'w');
	FILL_PERM(S_IXOTH, 'x');
#undef FILL_PERM
	perm->s[9] = '\0';
}

static void
format_time(char buffer[DATETIME_BUFSIZE], const time_t *timep)
{
	struct tm *tm;

	tm = localtime(timep);
	if (!tm) {
		strcpy(buffer, "<mtime error>");
	} else {
		strftime(buffer, DATETIME_BUFSIZE, "%b %d %H:%M", tm);
	}
}

int
main(void)
{
	DIR *current;
	struct dirent *entry;

	current = opendir(".");
	if (!current) {
		perror("opendir");
		return 1;
	}

	while ((entry = readdir(current))) {
		const char *name;
		struct stat sb;
		struct perm_s perm;
		struct passwd *passwd;
		struct group *group;
		char timebuf[DATETIME_BUFSIZE];
		int err;

		name = entry->d_name;
		if (!strcmp(".", name) || !strcmp("..", name)) {
			continue;
		}

		err = lstat(name, &sb);
		if (err) {
			perror(entry->d_name);
			continue;
		}
		format_permissions(&perm, sb.st_mode);
		printf("%c%s %d ",
		       	filetype_marker(sb.st_mode),
		       	perm.s,
		       	sb.st_nlink);

		// Probably, it could be useful to cache getpwuid()
		// result. On other hand, ls(1) is not meant to be used
		// in scripts, with huge input directories.
		passwd = getpwuid(sb.st_uid); 
		if (passwd) {
			printf("%s ", passwd->pw_name);
		} else {
			printf("%d ", sb.st_uid);
		}

		group = getgrgid(sb.st_gid);
		if (group) {
			printf("%s ", group->gr_name);
		} else {
			printf("%d ", sb.st_gid);
		}
		format_time(timebuf, &sb.st_mtime);
		printf("%s %s", timebuf, name);

		// Symbolic link can be arbitrary long, but truncating
		// to PATH_MAX seems reasonable. After all, you are not
		// going to use ls(1) in scripts, don't you?
		if (S_ISLNK(sb.st_mode)) {
			char buf[PATH_MAX]; 
			ssize_t nbytes;

			nbytes = readlink(name, buf, sizeof(buf));
			printf(" -> %s", buf);
			if (nbytes == sizeof(buf)) {
				printf(" [truncated]");
			}
		}
		putchar('\n');

	}
	closedir(current);
	return 0;
}

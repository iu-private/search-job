#include <stdbool.h>
#include "interface.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <stdio.h>

int
main(int argc, char **argv)
{
	int sock;
	int ret;
	struct sockaddr_un addr = { 0 };
	struct password pwd;
	pwd.data[0] = '7';
	pwd.data[1] = '4';
	pwd.data[2] = '8';
	pwd.data[3] = '2';
	pwd.data[4] = '5';
	pwd.data[5] = '3';

	srand(time(NULL));
	sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sock < 0) {
		perror("socket");
		return 1;
	}
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, SOCKET_PATH, sizeof(addr.sun_path) - 1);

	ret = connect(sock, (const struct sockaddr *)&addr,
			sizeof(struct sockaddr_un));
	if (ret == -1) {
		perror("connect");
		return 1;
	}
	client_protocol(sock, &pwd);
	close(sock);
	return 0;
}

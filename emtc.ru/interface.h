// Copyright 2018 Dmitry Bogatov <KAction@gnu.org>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*? <stdbool.h> */
#define SOCKET_PATH "emtc.ru.socket"

typedef char digit; // '0' .. '9'
struct pigeon {
	union {
		digit payload[5];
		double _force_8byte_alignment;
	};
};
struct password {
	digit data[6];
};

void write_pigeon(int fd, const struct pigeon *p);
bool read_pigeon(int fd, struct pigeon *p);
void write_password(int fd, const struct password *pwd);

void client_protocol(int fd, const struct password *pwd);
void server_protocol(int fd, struct password *pwd);

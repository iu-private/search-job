#include <stdbool.h>
#include "interface.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <stdio.h>

int
main(int argc, char **argv)
{
	struct sockaddr_un name = { 0 };
	int sock;
	int err;
	struct password pwd;
	int client;

	srand(time(NULL));
	sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sock < 0) {
		perror("socket");
		return 1;
	}

	unlink(SOCKET_PATH);

	name.sun_family = AF_UNIX;
	snprintf(name.sun_path, 4096, SOCKET_PATH);

	err = bind(sock, (const struct sockaddr *)&name, 
			 sizeof(struct sockaddr_un));
	if (err == -1) {
		perror("bind");
		return 1;
	}
	err = listen(sock, 20);
	if (err == -1) {
		perror("listen");
	}

	client = accept(sock, NULL, NULL);
	if (client < 0) {
		perror("accept");
		return 1;
	}
	server_protocol(client, &pwd);
	write_password(2, &pwd);

	close(client);
	close(sock);
	unlink(SOCKET_PATH);
}

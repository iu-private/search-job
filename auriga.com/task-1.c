/* The following code compiles on Linux. It has a number of problems,
 * however. Please locate as many of those problems as you are able and
 * provide your recommendations regarding how they can be resolved.
 */

// My solution to this task -- comments and suggestions are C99-style
// comments.
//
// Since formatting in your .doc document is stange (at least, as it is
// displayed by LibreOffice-6.0.1, I reformatted code to almost-Linux
// style.


typedef struct list_s {
	struct list_s *next;	/* NULL for the last item in a list */
	int data;
} list_t;

// According to POSIX, all names with _t suffix are reserved. Sorry, no
// link to standard, only to GNU Libc documentation:
// https://www.gnu.org/software/libc/manual/html_node/Reserved-Names.html
//
// And, by the way, this representation means that empty list is
// represented by NULL. Strange, but okay. Personally, I would prefer
// something like this:
//
//   struct node { int data; struct node *next; };
//   struct list { struct node *root; }

/* Counts the number of items in a list.  */
int
count_list_items(const list_t * head)
{
	if (head->next) {
		return count_list_items(head->next) + 1;
	} else {
		return 1;
	}
}

// This is recursive function, that consumes stack size proportional to
// length of the list. And, it does not handle case of head == NULL,
// which is they only way to support empty lists. Leaving list_t
// definition as-is, function body would be better written like
// following:
//
//   size_t len = 0;
//   while (head) {
//   	len += 1;
//   	head = head->next;
//   }
//   return len;


/* Inserts a new list item after the one specified as the argument. */
void insert_next_to_list(list_t *item, int data) 
{
	(item->next = malloc(sizeof(list_t)))->next = item->next;
	item->next->data = data;
}

// Too complicated, and have sequence point problems. Result of first assignment
// depends, whether LHS or RHS gets evaluated first. Why not simple:
//
// 	list_t *saved = item->next;
// 	list_t *new = malloc(sizeof(list_t));
// 	new->data = data;
// 	new->next = saved;
// 	item->next = new;
//
// Yes, I know, that 'new' is keyword in C++, but it is perfectly valid
// identifier in C.
//
// By the way, what happens if `malloc` returns NULL. This version (as
// is original) will crash on null pointer dereference. How to handle
// this case properly is hard question. I consider solution with `longjmp'
// on memory error much superiour to endless ENOMEM checks. 

/* Removes an item following the one specificed as the argument.  */
void
remove_next_from_list(list_t * item)
{
	if (item->next) {
		free(item->next);
		item->next = item->next->next;
	}
}

// Oblivious use-after-free error. Should be:
// 
// 	if (item->next) {
// 		list_t *saved = item->next->next;
// 		free(item->next);
// 		item->next = saved;
// 	}

/* Returns item data as text.  */
char *
item_data(const list_t * list)
{
	char buf[12];

	sprintf(buf, "%d", list->data);
	return buf;
}

// This function returns pointer to non-static local variable. This
// pointer is not valid after function return. As such, function should
// either malloc() memory or accept it as argument. I prefer later:
//
// 	char* item_data(char buf[12], const list_t *list)
//
// Unfortunately, compiler ignores [12] part of declaration, but it is
// still useful for human reader.

#define _POSIX_C_SOURCE 199309L
#include <stdbool.h>
#include "interface.h"

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>

#include <stdio.h>

static void
partly_recover_password(struct password *pwd, const struct pigeon *p)
{
	switch(p->payload[4]) {
	case '1':
		memcpy(pwd->data, p->payload, 4);
		break;
	case '2':
		memcpy(pwd->data + 2, p->payload, 4);
		break;
	case '3':
		memcpy(pwd->data, p->payload, 2);
		memcpy(pwd->data + 4, p->payload + 2, 2);
		break;
	default:
		assert(0 && "recover_password");
	}
}


static void
recover_password(struct password *pwd, 
		 const struct pigeon *fst, 
		 const struct pigeon *snd)
{
	partly_recover_password(pwd, fst);
	partly_recover_password(pwd, snd);
}

bool
read_pigeon(int fd, struct pigeon *p)
{
	return read(fd, p, sizeof(*p)) != -1;
}

void
write_pigeon(int fd, const struct pigeon *p)
{
	// XXX: check that write is successful
	if (rand() % 2) {
		write(fd, p, sizeof(*p));
	}
}

static void
wait_pigeons()
{
	struct timespec t; t.tv_sec = 0;
	t.tv_nsec = 1000;
	nanosleep(&t, NULL);
}

void
write_password(int fd, const struct password *pwd)
{
	printf("%c", pwd->data[0]);
	write(fd, pwd, sizeof(*pwd));
}

static void
init_pigeons(struct pigeon *pg, const struct password *pwd)
{
	for (int i = 0; i != 3; ++i) {
	       	(pg + i)->payload[4] = '1' + i;
	}
	/* First 4 digits. */
	memcpy(pg->payload, pwd->data, 4);
	/* Last 4 digits. */
	memcpy((pg + 1)->payload, pwd->data + 2, 4);
	/* First and last 2 digits */
	memcpy((pg + 2)->payload, pwd->data, 2);
	memcpy((pg + 2)->payload + 2, pwd->data + 4, 2);
}

void client_protocol(int fd, const struct password *pwd)
{
	struct pigeon pigeons[3];
	init_pigeons(pigeons, pwd);

	fcntl(fd, F_SETFL, O_NONBLOCK);

	/* Let us send data. */
	while (true) {
		struct pigeon reply;
		for (int i = 0; i != 3; ++i)
			write_pigeon(fd, pigeons + i);
		wait_pigeons();
		if (read_pigeon(fd, &reply)) {
			read_pigeon(fd, &reply);
			break;
		}
	}
}

void server_protocol(int fd, struct password *pwd)
{
	struct pigeon pigeons[3];
	struct pigeon ack;
	bool ack_sent;
	memset(&ack.payload, '0', 5);
	fcntl(fd, F_SETFL, O_NONBLOCK);

	while (true) {
		ssize_t bytes = read(fd, pigeons, sizeof(pigeons));
		if (ack_sent && bytes < 0) {
			break;
		} else {
			ack_sent = false;
		}
		if (bytes < 2 * sizeof(struct pigeon)) {
			continue;
		}
			
		recover_password(pwd, pigeons, pigeons + 1);
		write_pigeon(fd, &ack);
		write_pigeon(fd, &ack);
		ack_sent = true;

		wait_pigeons();
		wait_pigeons();
	}
}

